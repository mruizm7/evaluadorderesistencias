Item tmp;
Item barras[];
float xpos, ypos;
int anchoBarra;
int altoBarra;
String mensaje;
PFont pf;
boolean ban1;
float dx;
float dy;
int rs; // RECTANGULO SELECCIONADO

Resistencia r1;

void setup(){
    //pf = loadFont("Arial-BoldMT-32.vlw");
    //textFont(pf);
    textSize(18);
    size(700,400);
    xpos = width-300;
    ypos = 30;
    rs =0;
    anchoBarra = 20;
    altoBarra = 60;
    r1 = new Resistencia(180,170,140,altoBarra);
    tmp =null;
    barras = new Item[12];
    for(int i = 0;i < barras.length;i++){
       barras[i] = new Item(xpos+(i*20)+(5*i), ypos,anchoBarra, altoBarra, i);
    }
    mensaje = "algo";
}

void draw(){
    background(170);
    r1.dibujarResistencia();  // DIBUJA LA RESISTENCIA
    for(int i=0; i < barras.length;i++){
        barras[i].dibujarItem();
    }
    fill(0);
    text(mensaje+ " <= "+ban1,40,75);
    if (tmp != null){
        //tmp.yp = height-60;
        tmp.dibujarItem();
    }
}
void mousePressed(){
     mensaje = "SELECCIONA COLOR";
     for(int i=0;i < barras.length;i++){
        if( barras[i].isColision(mouseX, mouseY) ){
            ban1 = true;
            mensaje = barras[i].id;
            dx = mouseX-barras[i].xp;
            dy = mouseY-barras[i].yp;
            rs = barras[i].indiceColor;   
            tmp = new Item(barras[i].xp,barras[i].yp,
                      barras[i].ancho, barras[i].alto,
                      barras[i].indiceColor);
            break;
        }
       else{
         ban1 = false;
         mensaje = "SELECCIONA UN RECTANGULO";
   }
     }
}

void mouseDragged(){
  
    if(ban1){
       tmp.xp = mouseX-dx;
       tmp.yp = mouseY-dy;
    }
}


 void mouseReleased(){
    ban1 = false;
 }